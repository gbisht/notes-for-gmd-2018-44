## 1. Description

These MATALB scripts run Variably Saturated Flow Model (VSFM) (Bisht et al., 2018)
and compare VSFM results against published dataset or results from the [PFLOTRAN model]
(https://bitbucket.org/pflotran/pflotran/wiki/Home). Following benchmark problems are
presently supported:

1. Infiltartion in a dry 1D soil column (Celia et al. 1990)
2. Transient 1D infiltration in a two-layer soil (Srivastava and Yeh, 1991)
3. Transient 1D infiltration in a variably saturated soil column

The VSFM code is distributed via the [MPP library](https://github.com/MPP-LSM/MPP).

## 2. Installation instructions


### 2.1. Compiler requirments

The code requires:

- C compiler
- Fortran compiler
- Git
- CMake
- PETSc
- PFLOTRAN
- MPP

Set the following environment variables for installing PETSc, PFLOTRAN, and VSFM:

- `CC`     : Sequential C compiler
- `CXX`    : Sequential C++ compiler
- `FC`     : Sequential Fortran compiler

### 2.2. Install PETSc

Before installing VSFM or PFLOTRAN, one needs to install
[PETSc](http://www.mcs.anl.gov/petsc/).

```
cd <directory-of-choice>

# Clone PETSc
git clone https://bitbucket.org/petsc/petsc petsc

cd petsc

# Define an environment variable that stores the path to PETSc directory.
# This variable will be used in compile VSFM and PFLOTRAN. Additionally,
# this would used for running the MATLAB scripts.
export PETSC_DIR=$PWD

# Configure PETSc
./configure PETSC_ARCH=$PETSC_ARCH \
--with-cc=$CC \
--with-cxx=$CXX \
--with-fc=$FC \
--download-metis \
--download-parmetis \
--download-hdf5 \
--download-mpich \
--download-fblaslapack 

# Build PETSc
make PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH all

# Test installation of PETSc
make PETSC_DIR=$PETSC_DIR PETSC_ARCH=$PETSC_ARCH test
```

### 2.3. Install PFLOTRAN

```
cd <directory-of-choice>

# Clone PFLOTRAN
git clone https://bitbucket.org/pflotran/pflotran

cd pflotran

# This would used for running the MATLAB scripts.
export PETSC_DIR=$PWD

cd pflotran/src/pflotran

# Install PFLTORAN
make pflotran

# Test installation of PFLOTRAN
make test
```

### 2.4. Install VSFM

```
cd <directory-of-choice>

# Clone the MPP
git clone https://github.com/MPP-LSM/MPP

cd MPP

# This would used for running the MATLAB scripts.
export MPP_DIR=$PWD

# Configure MPP
make config

# Install MPP
make install

# Test installation of MPP
make test
```

### 2.5. Run the MATLAB scripts


1. Download MATLAB code
```
git clone https://bitbucket.org/gbisht/notes-for-gmd-2018-44

```

2. Launch MALTAB and change directory to the top level notes-for-gmd-2018-44 directory
```
>> cd <notes-for-gmd-2018-44-directory>
```

3. Define variables that are define path to PETSc, PFLTORAN, and MPP directories.
```
>>MPP_DIR      ='<directory-of-mpp>';
>>PFLOTRAN_DIR ='<directory-of-pflotran>';
>>PETSC_DIR    ='<directory-of-petsc>';
>>
```

4. Run a benchmark problem

```
>> run_benchmark_problem(1,MPP_DIR,PFLOTRAN_DIR,PETSC_DIR)
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
          Celia et al. (1980) benchmark problem
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
 
  Running VSFM for the Celia et al. (1980) benchmark problem
    cd /Users/gbisht/projects/acme/vsfm/notes-for-gmd-2018-44/benchmark_problem_1/vsfm_output
    /Users/gbisht/projects/acme/vsfm/notes-for-gmd-2018-44/MPP/local/bin/vsfm_celia1990 -dt    180 -nstep 480 -nz   1000  -save_initial_soln  -save_final_soln    -output_suffix celia

```

Example of the VSFM comparision against Celia et al. (1980) problem is shown below:

![vsfm_benchmark_1](vsfm_benchmark_1.png)


## References

Bisht, G., Riley, W. J., Hammond, G. E., and Lorenzetti, D. M.: Development and evaluation of
a variably saturated flow model in the global E3SM Land Model (ELM) version 1.0, 
Geosci. Model Dev., 11, 4085-4102, https://doi.org/10.5194/gmd-11-4085-2018, 2018.


Celia, M. A., Bouloutas, E. T., & Zarba, R. L. (1990). A general mass‐conservative 
numerical solution for the unsaturated flow equation. Water resources research, 26(7), 
1483-1496.

Srivastava, R., & Yeh, T. C. J. (1991). Analytical solutions for one‐dimensional,
transient infiltration toward the water table in homogeneous and layered soils. 
Water Resources Research, 27(5), 753-762.




