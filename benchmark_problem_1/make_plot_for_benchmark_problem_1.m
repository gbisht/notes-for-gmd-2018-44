function make_plot_for_benchmark_problem_1(PETSC_DIR, fig_dir)
%

dirname = pwd;addpath([dirname '/../utils']);
addpath ([PETSC_DIR '/share/petsc/matlab']);

celia_data = csvread('celia_data.csv',1,0);

% Load VSFM's presure binary output
p_initial = PetscBinaryRead('vsfm_output/initial_soln_celia.bin');
p_final   = PetscBinaryRead('vsfm_output/final_soln_celia.bin');

% Convert VSFM pressure from Pa to m
p_initial = Convert_Pressure_2_Head(p_initial);
p_final   = Convert_Pressure_2_Head(p_final);

% Compute the 1D mesh
dz = 1/length(p_final);
zz = [1:-dz:dz/2];

% Make the plot
figure;
plot(p_final, zz,'linewidth',2)
hold all;
plot(celia_data(:,2), celia_data(:,1),'sr','markerfacecolor','r','markersize',8)
plot(p_initial, zz ,'-g','linewidth',2)
set(gca,'fontweight','bold','fontsize',16)
set(gca,'ydir','reverse')
ylim([0 1])
xlabel('Pressure head [m]')
ylabel('Depth [m]')
h=legend('VSFM','Celia et al. (1980)','location','southeast');

set(h,'fontsize',14)
orient landscape 
print('-dpdf', [fig_dir '/benchmark_problem_1.pdf']);

rmpath([dirname '/../utils']);
rmpath ([PETSC_DIR '/share/petsc/matlab']);
