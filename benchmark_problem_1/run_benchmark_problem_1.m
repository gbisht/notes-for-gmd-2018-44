function run_benchmark_problem_1(MPP_DIR, PETSC_DIR, fig_dir)

disp('')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
disp('')
disp('          Celia et al. (1980) benchmark problem')
disp('')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

% Run the VSFM problem
run_vsfm_benchmark_problem_1(MPP_DIR);

% Make the comparison plot
make_plot_for_benchmark_problem_1(PETSC_DIR, fig_dir);

% Remove unwanted files
system('rm -rf vsfm_output');
