function run_vsfm_benchmark_problem_1(MPP_DIR)

disp(' ')
disp('  Running VSFM for the Celia et al. (1980) benchmark problem');

dirname = pwd; addpath([dirname '/../utils']);

system('rm -rf vsfm_output');
system('mkdir -p vsfm_output');

cd vsfm_output

exe_name = 'vsfm_celia1990';
problem_options = [...
    '-dt    180 ' ... % Time step [s]
    '-nstep 480 ' ... % Number of time steps
    '-nz   1000 ' ... % Number of vertical grid cells
    ];

output_options = [ ...
    '-save_initial_soln  ' ... % Save initial pressure profile
    '-save_final_soln    ' ... % Save final pressure profile
    '-output_suffix celia' ... % Add suffix 'celia' to binary files
    ];

vsfm_cmd = [exe_name ' ' problem_options ' ' output_options];

run_vsfm_problem(vsfm_cmd,MPP_DIR)

cd ../
rmpath([dirname '/../utils']);