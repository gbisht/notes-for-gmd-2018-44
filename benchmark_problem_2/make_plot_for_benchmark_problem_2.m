function make_plot_for_benchmark_problem_2(PETSC_DIR, fig_dir)

dirname = pwd;addpath([dirname '/../utils']);
addpath ([PETSC_DIR '/share/petsc/matlab']);

cyan = [0 0.8 0.8];
green = [0 0.5 0];

FILENAME = 'pflotran/wetting.h5';
z_h = h5read(FILENAME,'/Coordinates/Z [m]');
z = flipud(0.5*(z_h(1:end-1)+z_h(2:end)));

pf_wet_000hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  0.00000E+00 s/Liquid_Pressure [Pa]'));
pf_wet_001hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  3.60000E+03 s/Liquid_Pressure [Pa]'));
pf_wet_005hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  1.80000E+04 s/Liquid_Pressure [Pa]'));
pf_wet_010hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  3.60000E+04 s/Liquid_Pressure [Pa]'));
pf_wet_020hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  7.20000E+04 s/Liquid_Pressure [Pa]'));
pf_wet_100hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  3.60000E+05 s/Liquid_Pressure [Pa]'));

FILENAME = 'pflotran/drying.h5';
pf_dry_000hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  0.00000E+00 s/Liquid_Pressure [Pa]'));
pf_dry_001hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  3.60000E+03 s/Liquid_Pressure [Pa]'));
pf_dry_005hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  1.80000E+04 s/Liquid_Pressure [Pa]'));
pf_dry_010hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  3.60000E+04 s/Liquid_Pressure [Pa]'));
pf_dry_020hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  7.20000E+04 s/Liquid_Pressure [Pa]'));
pf_dry_100hr = Convert_Pressure_2_Head(h5read(FILENAME,'/Time:  3.60000E+05 s/Liquid_Pressure [Pa]'));

vsfm_wet_000hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/initial_soln_wet_001hr.bin'));
vsfm_wet_001hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/final_soln_wet_001hr.bin'));
vsfm_wet_005hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/final_soln_wet_005hr.bin'));
vsfm_wet_010hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/final_soln_wet_010hr.bin'));
vsfm_wet_020hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/final_soln_wet_020hr.bin'));
vsfm_wet_100hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/final_soln_wet_100hr.bin'));

vsfm_dry_000hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/initial_soln_dry_001hr.bin'));
vsfm_dry_001hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/final_soln_dry_001hr.bin'));
vsfm_dry_005hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/final_soln_dry_005hr.bin'));
vsfm_dry_010hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/final_soln_dry_010hr.bin'));
vsfm_dry_020hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/final_soln_dry_020hr.bin'));
vsfm_dry_100hr = Convert_Pressure_2_Head(PetscBinaryRead('vsfm_output/final_soln_dry_100hr.bin'));


figure;
clf
idx = [1:10:200];
zc8 = z;
zc9 = z;

subplot(1,2,1);
plot(vsfm_wet_000hr,zc9,'-b','linewidth',2)
hold all
plot(pf_wet_000hr(idx),zc8(idx),'bs');
plot(vsfm_wet_001hr,zc9,'-r','linewidth',2)
plot(vsfm_wet_005hr,zc9,'-','linewidth',2,'color',cyan)
plot(vsfm_wet_010hr,zc9,'-m','linewidth',2)
plot(vsfm_wet_020hr,zc9,'-','linewidth',2,'color',green)
plot(vsfm_wet_100hr,zc9,'-k','linewidth',2)

plot(pf_wet_001hr(idx),zc8(idx),'-rs');
plot(pf_wet_005hr(idx),zc8(idx),'-s','color',cyan);
plot(pf_wet_010hr(idx),zc8(idx),'-ms');
plot(pf_wet_020hr(idx),zc8(idx),'-s','color',green);
plot(pf_wet_100hr(idx),zc8(idx),'-ks')

xlim([-0.5 0])
set(gca,'fontweight','bold','fontsize',14)
set(gca,'xtick',[-0.5:.1:0])

patch([-0.5 0 0 -0.5 -0.5],[0 0 1 1 0],[0.5 0.5 0.5],'FaceAlpha',0.5)

text(-0.45,1.4,'time = 0 [hr]','color','b','fontsize',12,'fontweight','bold')
text(-0.45,1.5,'time = 1 [hr]','color','r','fontsize',12,'fontweight','bold')
text(-0.45,1.6,'time = 5 [hr]','color',cyan,'fontsize',12,'fontweight','bold')
text(-0.45,1.7,'time =  10 [hr]','color','m','fontsize',12,'fontweight','bold')
text(-0.45,1.8,'time =  20 [hr]','color',green,'fontsize',12,'fontweight','bold')
text(-0.45,1.9,'time = 100 [hr]','color','k','fontsize',12,'fontweight','bold')

set(gca,'ydir','reverse')

ylabel('Depth [m]')
xlabel('Pressure head [m]')
title('(a) Wetting scenario')
h = legend('VSFM','PFLOTRAN','location','southoutside','orientation','horizontal');

subplot(1,2,2);
plot(vsfm_dry_000hr,zc9,'-b','linewidth',2) % 0 hr
hold all
plot(pf_dry_000hr(idx),zc9(idx),'bs'); % 0hr

plot(vsfm_dry_001hr,zc9,'-r','linewidth',2) % 1 hr
plot(vsfm_dry_005hr,zc9,'-','linewidth',2,'color',cyan) % 5 hr
plot(vsfm_dry_010hr,zc9,'-m','linewidth',2) % 10 hr
plot(vsfm_dry_020hr,zc9,'-','linewidth',2,'color',green) % 20 hr
plot(vsfm_dry_100hr,zc9,'-k','linewidth',2) % 100 hr


plot(pf_dry_001hr(idx),zc9(idx),'rs'); % 1hr
hold all;
plot(pf_dry_005hr(idx),zc9(idx),'s','color',cyan); % 5hr
plot(pf_dry_010hr(idx),zc9(idx),'ms'); % 10hr
plot(pf_dry_020hr(idx),zc9(idx),'s','color',green); % 20hr
plot(pf_dry_100hr(idx),zc9(idx),'ks') % 100 hr
set(gca,'fontweight','bold','fontsize',14)

% a = annotation('textarrow', [0.8 0.70], [0.65 0.50],'String','Time');
% set(a,'fontweight','bold','fontsize',14)             

xlim([-0.5 0])
set(gca,'xtick',[-0.5:.1:0])

text(-0.45,1.4,'time = 0 [hr]','color','b','fontsize',12,'fontweight','bold')
text(-0.45,1.5,'time = 1 [hr]','color','r','fontsize',12,'fontweight','bold')
text(-0.45,1.6,'time = 5 [hr]','color',cyan,'fontsize',12,'fontweight','bold')
text(-0.45,1.7,'time =  10 [hr]','color','m','fontsize',12,'fontweight','bold')
text(-0.45,1.8,'time =  20 [hr]','color',green,'fontsize',12,'fontweight','bold')
text(-0.45,1.9,'time = 100 [hr]','color','k','fontsize',12,'fontweight','bold')

set(gca,'ydir','reverse')


h = legend('VSFM','PFLOTRAN','location','southoutside','orientation','horizontal');

patch([-0.5 0 0 -0.5 -0.5],[0 0 1 1 0],[0.5 0.5 0.5],'FaceAlpha',0.5)

xlabel('Pressure head [m]')
title('(b) Drying scenario')

orient landscape 
print('-dpdf', [fig_dir '/benchmark_problem_2.pdf']);

rmpath([dirname '/../utils']);
addpath ([PETSC_DIR '/share/petsc/matlab']);
