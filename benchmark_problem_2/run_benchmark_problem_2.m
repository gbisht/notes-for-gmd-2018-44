function run_benchmark_problem_2(MPP_DIR, PFLOTRAN_DIR, PETSC_DIR, fig_dir)

disp('')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
disp('')
disp('    Transient variably saturated 1D benchmark problem     ')
disp('')
disp('++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')

% Run the VSFM problem
run_vsfm_benchmark_problem_2(MPP_DIR);

% Run the VSFM problem
run_pflotran_benchmark_problem_2(PFLOTRAN_DIR);

% Make the comparison plot
make_plot_for_benchmark_problem_2(PETSC_DIR, fig_dir);

% Remove unwanted files
system('rm -rf vsfm_output pflotran/wetting.h5 pflotran/drying.h5 pflotran/*.txt pflotran/*.out');
