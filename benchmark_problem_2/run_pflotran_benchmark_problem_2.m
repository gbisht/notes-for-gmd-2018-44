function run_pflotran_benchmark_problem_2(PFLOTRAN_DIR)

global verbose

cd pflotran

cmd_txt = [PFLOTRAN_DIR '/src/pflotran/pflotran -pflotranin drying.in > pflotran_screen_output.txt'];

disp(' ')
disp('  Running PFLOTRAN for the transient variably saturated 1D soil system');

if (verbose)
    dirname = pwd;
    disp('')
    disp(['    cd ' dirname]);
    disp(['    ' cmd_txt]);
end

status = system(cmd_txt);

if (status)
    error('  ERROR: Run did not finish correctly');
end

cmd_txt = [PFLOTRAN_DIR '/src/pflotran/pflotran -pflotranin wetting.in > pflotran_screen_output.txt'];

disp(' ')
disp('  Running PFLOTRAN for the transient variably saturated 1D soil system');

if (verbose)
    dirname = pwd;
    disp('')
    disp(['    cd ' dirname]);
    disp(['    ' cmd_txt]);
end

status = system(cmd_txt);

if (status)
    error('  ERROR: Run did not finish correctly');
end

cd ../



% cd benchmark_problem_2/pflotran
% 
% status = system('/Users/gbisht/projects/acme/vsfm/notes-for-gmd-2018-44/PFLOTRAN/src/pflotran/pflotran -pflotranin drying.in');
% if ~status; 
%     error('Unable to run PFLOTRAN on the benchmark problem 2 drying scenario'); 
% end
%     
% status = system('/Users/gbisht/projects/acme/vsfm/notes-for-gmd-2018-44/PFLOTRAN/src/pflotran/pflotran -pflotranin wetting.in');
% if ~status; 
%     error('Unable to run PFLOTRAN on the benchmark problem 2 wetting scenario'); 
% end
% 
% cd ../../
    