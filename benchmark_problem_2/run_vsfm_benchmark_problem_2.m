
function run_vsfm_benchmark_problem_2(MPP_DIR)

disp(' ')
disp('  Running VSFM for the Srivastava and Yeh (1991) transient benchmark problems');

dirname = pwd;addpath([dirname '/../utils']);

system('rm -rf vsfm_output');
system('mkdir -p vsfm_output');
cd vsfm_output

WETTING_PROBLEM = 1;
DRYING_PROBLEM  = 2;

nsteps        = [ 36     180      360     720     3600  ];
time_suffixes = {'001hr' '005hr' '010hr' '020hr' '100hr'};
prob_suffixes = {'wet' 'dry'};

exe_name = 'vsfm_sy1991';

for iprob = [WETTING_PROBLEM DRYING_PROBLEM]
    
    for istep = 1:length(time_suffixes)
        
        nstep = nsteps(istep);
        
        suffix = [prob_suffixes{iprob} '_' time_suffixes{istep}];
        
        problem_options = [...
            '-dt    100 '               ... % Time step [s]
            sprintf('-nstep %04d ',nstep) ... % Number of time steps
            ];
        
        output_options = [ ...
            sprintf('-problem_number %d ',iprob) ... % Problem identifier
            '-save_initial_soln  '               ... % Save initial pressure profile
            '-save_final_soln    '               ... % Save final pressure profile
            '-output_suffix ' suffix             ... % Add suffix to binary output files
            ];
        
        vsfm_cmd = [exe_name ' ' problem_options ' ' output_options];
                
        run_vsfm_problem(vsfm_cmd, MPP_DIR)
        disp(' ');
        
    end
end

cd ../
addpath([dirname '/../utils']);
