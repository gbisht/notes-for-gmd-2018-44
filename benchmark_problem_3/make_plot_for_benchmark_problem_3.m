function make_plot_for_benchmark_problem_3(PETSC_DIR, fig_dir)

dirname = pwd;addpath([dirname '/../utils']);
addpath ([PETSC_DIR '/share/petsc/matlab']);

% Read VSFM output
vsfm_p0 = PetscBinaryRead('vsfm_output/initial_soln_wt.bin');
vsfm_p1 = PetscBinaryRead('vsfm_output/final_soln_wt.bin');
vsfm_s0 = PetscBinaryRead('vsfm_output/initial_sat_wt.bin');
vsfm_s1 = PetscBinaryRead('vsfm_output/final_sat_wt.bin');

% Read PFLOTRAN output
pf_p0 = h5read('pflotran/wt_dynamics.h5','/Time:  0.00000E+00 d/Liquid_Pressure [Pa]');
pf_p1 = h5read('pflotran/wt_dynamics.h5','/Time:  1.00000E+00 d/Liquid_Pressure [Pa]');
pf_s0 = h5read('pflotran/wt_dynamics.h5','/Time:  0.00000E+00 d/Liquid_Saturation');
pf_s1 = h5read('pflotran/wt_dynamics.h5','/Time:  1.00000E+00 d/Liquid_Saturation');

% Convert Pa to m
vsfm_p0 = Convert_Pressure_2_Head(vsfm_p0);
vsfm_p1 = Convert_Pressure_2_Head(vsfm_p1);
pf_p0   = Convert_Pressure_2_Head(pf_p0);
pf_p1   = Convert_Pressure_2_Head(pf_p1);

% Compute the 1D mesh
dz = 1/length(vsfm_p0);
zz = [1:-dz:dz/2];

% Make the plot
figure;
subplot(1,2,1)
plot(vsfm_p0,zz,'-b','linewidth',2)
hold all
plot(pf_p0(1:10:end),zz(1:10:end),'sb','linewidth',2)
plot(vsfm_p1   ,zz,'-r','linewidth',2)
plot(pf_p1(1:10:end),zz(1:10:end),'sr','linewidth',2)
set(gca,'ydir','reverse')

set(gca,'fontweight','bold','fontsize',14)
grid on
ylabel('Depth [m]')
xlabel('Pressure head [m]')
h = legend(...
    'VSFM at t = 0 [day]',...
    'PFLOTRAN at t = 0 [day]',...
    'VSFM at t = 1 [day]',...
    'PFLOTRAN at t = 1 [day]',...
    'location','southwest','orientation','vertical');
title('(a)')

subplot(1,2,2)
plot(vsfm_s0,zz,'-b','linewidth',2)
hold all
plot(pf_s0(1:10:end),zz(1:10:end),'bs','linewidth',2)
plot(vsfm_s1,zz,'-r','linewidth',2)
plot(pf_s1(1:10:end),zz(1:10:end),'rs','linewidth',2)

set(gca,'ydir','reverse')

set(gca,'fontweight','bold','fontsize',14)
grid on
xlabel('Saturation [-]')
h = legend(...
    'VSFM at t = 0 [day]',...
    'PFLOTRAN at t = 0 [day]',...
    'VSFM at t = 1 [day]',...
    'PFLOTRAN at t = 1 [day]',...
    'location','southwest','orientation','vertical');
title('(b)')

orient landscape 
print('-dpdf', [fig_dir '/benchmark_problem_3.pdf']);

rmpath([dirname '/../utils']);
rmpath ([PETSC_DIR '/share/petsc/matlab']);
