function run_pflotran_benchmark_problem_3(PFLOTRAN_DIR)

global verbose

cd pflotran

cmd_txt = [PFLOTRAN_DIR '/src/pflotran/pflotran -pflotranin wt_dynamics.in > pflotran_screen_output.txt'];

disp(' ')
disp('  Running PFLOTRAN for the transient variably saturated 1D soil system');

if (verbose)
    dirname = pwd;
    disp('')
    disp(['    cd ' dirname]);
    disp(['    ' cmd_txt]);
end

status = system(cmd_txt);

if (status)
    error('  ERROR: Run did not finish correctly');
end

cd ../