function run_vsfm_benchmark_problem_3(MPP_DIR)

disp(' ')
disp('  Running VSFM for the transient variably saturated 1D soil system');

dirname = pwd; addpath([dirname '/../utils']);

system('rm -rf vsfm_output');
system('mkdir -p vsfm_output');
cd vsfm_output

exe_name = 'vsfm_wt_dynamics';

problem_options = [...
    '-dt    3600 ' ... % Time step [s]
    '-nstep 24   ' ... % Number of time steps
    ];

output_options = [ ...
    '-save_initial_soln  ' ... % Save initial pressure profile
    '-save_initial_sat  ' ... % Save initial pressure profile
    '-save_final_soln    ' ... % Save final pressure profile
    '-save_final_sat    ' ... % Save final pressure profile
    '-output_suffix wt   ' ... % Add suffix to binary output files
    ];

vsfm_cmd = [exe_name ' ' problem_options ' ' output_options];

run_vsfm_problem(vsfm_cmd,MPP_DIR)

cd ../

rmpath([dirname '/../utils']);