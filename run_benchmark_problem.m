function run_benchmark_problem(ii, mpp_dir, pflotran_dir, petsc_dir, fig_dir)
%
% RUN_BENCHMARK_PROBLEM  Runs VSFM benchmark problem and make comparison
%   plots aganist dataset or PFLOTRAN results.
%
%   RUN_BENCHMARK_PROBLEM(ii, mpp_dir, pflotran_dir, petsc_dir) runs ii-th
%   benchmark problem. The directory for MPP, PFLOTRAN, and PETSc are
%   defined by mpp_dir, pflotran_dir, and petsc_dir input arguments.
%
%   Valid values for ii-th benchmark problem are:
%     0 All supported benchmark problems
%     1 Celia et al. (1980) benchmark problem.
%     2 Srivastava and Yeh (1991) transient benchmark problem in a
%       2-layer soil system.
%     3 Transient variably saturated 1D soil system
%
%

ALL_PROBLEMS  = 0;
PROBLEM_CELIA = 1;
PROBLEM_SY    = 2;
PROBLEM_WT    = 3;

switch ii
    case ALL_PROBLEMS
        cd benchmark_problem_1
        run_benchmark_problem_1(mpp_dir,petsc_dir,fig_dir);
        cd ../
        
        cd benchmark_problem_2
        run_benchmark_problem_2(mpp_dir, pflotran_dir, petsc_dir, fig_dir);
        cd ../

        cd benchmark_problem_3
        run_benchmark_problem_3(mpp_dir, pflotran_dir, petsc_dir, fig_dir);
        cd ../
        
    case PROBLEM_CELIA
        cd benchmark_problem_1
        run_benchmark_problem_1(mpp_dir, petsc_dir, fig_dir);
        cd ../
                
    case PROBLEM_SY
        cd benchmark_problem_2
        run_benchmark_problem_2(mpp_dir, pflotran_dir, petsc_dir, fig_dir);
        cd ../
        
    case PROBLEM_WT
        cd benchmark_problem_3
        run_benchmark_problem_3(mpp_dir, pflotran_dir, petsc_dir, fig_dir);
        cd ../
end

