function Head = Convert_Pressure_2_Head(Pressure)

REF_PRESSURE = 101325;  % [Pa]
H2O_DENSITY  = 997.16;  % [kg/m^3]
GRAVITY      = 9.80868; % [m/s^2}

Head = (Pressure - REF_PRESSURE)/H2O_DENSITY/GRAVITY;

