function run_vsfm_problem(vsfm_cmd,MPP_DIR)

global verbose

cmd_txt = [MPP_DIR '/local/bin/' vsfm_cmd];

if (verbose)
    dirname = pwd;
    disp('')
    disp(['    cd ' dirname]);
    disp(['    ' cmd_txt]);
end

% Run the VSFM model
status = system(cmd_txt);

if (status)
    error('  ERROR: Run did not finish correctly');
end


